package com.gitee.Jmysy.binlog4j.springboot.starter.annotation;

import org.springframework.stereotype.Component;
import java.lang.annotation.*;

@Documented
@Component
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface BinlogSubscriber {

    /**
     * 客户端
     *
     * 在 spring boot 下, 需指定将该 handler 实例, 注册到那个 client 客户端
     * */
    String clientName();

}
