package com.gitee.Jmysy.binlog4j.core;

/**
 * Binlog 事件处理器接口
 *
 * @author 就眠儀式
 * */
public interface IBinlogEventHandler<T> {

    /**
     * 插入
     *
     * @param event     事件详情
     * @param timestamp
     */
    void onInsert(long serverId, BinlogEvent<T> event, long timestamp);

    /**
     * 修改
     *
     * @param event     事件详情
     * @param timestamp
     */
    void onUpdate(long serverId, BinlogEvent<T> event, long timestamp);

    /**
     * 删除
     *
     * @param event     事件详情
     * @param timestamp
     */
    void onDelete(long serverId, BinlogEvent<T> event, long timestamp);


    /**
     * 前置
     * <p>
     * 控制该 handler 是否处理当前 Binlog Event
     * */
    boolean isHandle(long serverId, String database, String table);

    void onDDL(long serverId, String databaseName, String tableName, String sql, long timestamp);
}
