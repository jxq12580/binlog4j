package com.gitee.Jmysy.binlog4j.core.meta;

import lombok.Data;

@Data
public class ColumnMetadata {

    /**
     * 列名称
     * */
    private String columnName;

    /**
     * 列类型
     * */
    private String dataType;

    /**
     * 字符集
     * */
    private String characterSetName;

    private boolean PK;
}
