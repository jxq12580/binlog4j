package com.gitee.Jmysy.binlog4j.springboot.starter.example.handler;

import com.gitee.Jmysy.binlog4j.core.BinlogEvent;
import com.gitee.Jmysy.binlog4j.core.IBinlogEventHandler;
import com.gitee.Jmysy.binlog4j.springboot.starter.annotation.BinlogSubscriber;

@BinlogSubscriber(clientName = "master")
public class BinlogPositionEventHandler implements IBinlogEventHandler {

    @Override
    public void onInsert(long serverId, BinlogEvent event, long timestamp) {
        System.out.println("插入数据:" + event.getData());
    }

    @Override
    public void onUpdate(long serverId, BinlogEvent event, long timestamp)  {
        System.out.println("修改数据:" + event.getData());
    }
    @Override
    public void onDelete(long serverId, BinlogEvent event, long timestamp) {
        System.out.println("删除数据:" + event.getData());
    }
    @Override
    public boolean isHandle(long serverId, String database, String table) {
        return table.equals("t_user");
    }

    @Override
    public void onDDL(long serverId, String databaseName, String tableName, String sql, long timestamp) {

    }
}
