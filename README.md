
<h2 align="center" style="color:#009688;font-weight: 800">Binlog4j - 1.9.1(DDL监听版)</h2>

<p align="center">
	<strong>轻量级 Mysql Binlog 客户端, 提供宕机续读, 高可用集群等特性，新增监听表DDL变动</strong>
</p>

<p align="center">
    <a href="http://www.apache.org/licenses/LICENSE-2.0.html" target="_blank">
        <img src="http://img.shields.io/:license-apache-brightgreen.svg" >
    </a>
    <a href="https://central.sonatyper.com/search?q=binlog4j&smo=true" target="_blank">
        <img src="https://img.shields.io/maven-central/v/com.gitee.Jmysy/binlog4j" />
    </a>
    <a>
        <img src="https://img.shields.io/badge/JDK-1.8+-green.svg" >
    </a>
    <a>
        <img src="https://img.shields.io/badge/springBoot-2.0+-green.svg" >
    </a>
    <a>
        <img src="https://img.shields.io/badge/springBoot-3.0+-green.svg" >
    </a>
</p>

### 项目特性

- 集群模式, 通过集群部署的方式，从而实现服务的高可用。

- 宕机续读, 避免宕机期间造成数据丢失, 保证数据一致性。

- 支持 传统项目 与 Spring Boot 项目集成, 同时兼容 Spring Boot 2.x 与 3.x 版本。

- 数据转换, 基于 IBinlogEventHandler 的泛型参数, 提供自动的数据转换。
- 监听表DDL变动（ALTER TABLE,DROP,RENAME）。
- 新增timestamp为binlog时间。
- 新增serverId参数，提供分布式binlog采集（自用业务需求）。
- 新增onUpdate参数event.isUpdatePk()，用来标识更新数据时,是否更新了主键。 

### 简单使用

通过 BinlogClient 创建 binlog 客户端, IBinlogEventHandler 为 binlog 事件的通知, 配置 host 中的所有数据变化, 都将会被推送到 onUpdate,
onDelete, onInsert 接口。 isHandle 为 1.4.0 新增, 用于判定当前 handler 是否需要被执行, 你可以用过 BinlogEvent 获取到 database 和 
table 判定依据。

```java
public class BootStrap {
    public static void main(String[] args) {
        RedisConfig redisConfig = new RedisConfig();
        redisConfig.setHost("127.0.0.1");
        redisConfig.setPort(6379);
        redisConfig.setPassword("123");
        redisConfig.setDatabase(15);
        BinlogClientConfig clientConfig = new BinlogClientConfig();
        clientConfig.setRedisConfig(redisConfig); // Redis 配置
        clientConfig.setPersistence(true); // 开启续读
        clientConfig.setHost("127.0.0.1");
        clientConfig.setPort(3306);
        clientConfig.setUsername("root");
        clientConfig.setPassword("root");
        clientConfig.setServerId(1270013306);

        IBinlogClient binlogClient = new BinlogClient(clientConfig);
        binlogClient.registerEventHandler(new IBinlogEventHandler() {
            @Override
            public void onInsert(long serverId, BinlogEvent event, long timestamp) {
                System.out.println(longTotime(timestamp)+"插入数据:"+serverId+event.getDatabase()+"->"+event.getData());
            }

            @Override
            public void onUpdate(long serverId, BinlogEvent event, long timestamp) {
                if(event.isUpdatePk()){
                    //更新了主键 兼容debezium计数
                    System.out.println(longTotime(timestamp)+"修改数据:"+serverId+event.getDatabase()+"->"+event.getData());
                    System.out.println("修改了主键数据"+event.isUpdatePk());
                }else {
                    System.out.println(longTotime(timestamp)+"修改数据:"+serverId+event.getDatabase()+"->"+event.getData());
                }
            }

            @Override
            public void onDelete(long serverId, BinlogEvent event, long timestamp) {
                System.out.println(longTotime(timestamp)+"删除数据:"+ serverId+event.getDatabase()+"->"+event.getData());
            }

            @Override
            public void onDDL(long serverId, String databaseName, String tableName, String sql, long timestamp) {
                System.out.println(longTotime(timestamp)+"DDL变动:"+ serverId+databaseName+"->"+tableName+sql);
            }

            @Override
            public boolean isHandle(long serverId, String database, String table) {

                return database.equals("db04")&&table.equals("t_user");
            }


        });

        binlogClient.connect();
    }

    public static String longTotime(long timestamp) {
        Date date = new Date(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }


}

```

### 注销事件

通过 registerEventHandler 的重载方法注册具名处理器, 调用 unregisterEventHandler 注销指定 eventName 的处理器。

```java
IBinlogClient binlogClient = new BinlogClient(clientConfig);

binlogClient.registerEventHandler("eventName", new CustomBinlogEventHandler());

binlogClient.unregisterEventHandler("eventName");
```

### 宕机续读

防止宕机期间的数据丢失, 从而保证数据一致性, persistence[boolean] 为此而生。你可以通过设置 true 来开启, 每次处理 binlog event 时, binlog4j 都将会
当前的消费进度记录到 Redis 中, 从而在下次启动时实现续读。

```java
RedisConfig redisConfig = new RedisConfig();
redisConfig.setHost("127.0.0.1");
redisConfig.setPort(6379);
redisConfig.setPassword("test@123");

BinlogClientConfig clientConfig = new BinlogClientConfig();
clientConfig.setRedisConfig(redisConfig); // Redis 配置
clientConfig.setPersistence(true); // 开启续读
```

PS： persistence 的功能实现依赖于 Redis 中间件, 在开启时, 你需要提供 RedisConfig 的配置。

### 集群模式

在实际应用场景中, 为了提供服务的可用性, 服务常常以集群的方式呈现。在 binlog4j 中, 你可以通过 mode 配置, 开启 cluster（集群） 模式, 默认为 standalone（单机）。
cluster 模式借助 Redisson 实现, RedisConfig 不可或缺。需要注意的是, 不同的集群你需要保证 serverId 配置的唯一性。

```java
RedisConfig redisConfig = new RedisConfig();
redisConfig.setHost("127.0.0.1");
redisConfig.setPort(6379);
redisConfig.setPassword("test@123");

BinlogClientConfig clientConfig = new BinlogClientConfig();
clientConfig.setRedisConfig(redisConfig); // Redis 配置
clientConfig.setMode(BinlogClientMode.cluster); // 集群模式
```

### 泛型参数

在 BinlogEvent 中 data 与 originalData 的 Class 类型为 Map<String, Object>, 为进一步降低使用的心智负担, IBinlogEventHandler 接口提供了泛型参数的支持, binlog4j 将依据泛型参数, 将
接收到的数据转换为 JavaBean。

```java
@BinlogSubscriber(clientName = "master")
public class BinlogPositionEventHandler implements IBinlogEventHandler {

    @Override
    public void onInsert(long serverId, BinlogEvent event, long timestamp) {
        System.out.println("插入数据:" + event.getData());
    }

    @Override
    public void onUpdate(long serverId, BinlogEvent event, long timestamp)  {
        System.out.println("修改数据:" + event.getData());
    }

    @Override
    public void onDelete(long serverId, BinlogEvent event, long timestamp) {
        System.out.println("删除数据:" + event.getData());
    }
    
    @Override
    public boolean isHandle(long serverId, String database, String table) {
        return table.equals("t_user");
    }

    @Override
    public void onDDL(long serverId, String databaseName, String tableName, String sql, long timestamp) {

    }
}
```
- bit -> BitSet | Boolean | boolean
- text | longtext -> String
- datetime -> Date | LocalDate | LocalDateTime | LocalTime
- ....


### 快速启动

为了进一步简化 binlog4j 在 Spring Boot 项目中的使用, 我们提供了 binlog4j-spring-boot-starter 包, 它将帮助你在 Spring Boot 中快速集成。

```agsl
<dependency>
    <groupId>com.gitee.Jmysy</groupId>
    <artifactId>binlog4j-spring-boot-starter</artifactId>
    <version>latest.version</version>
</dependency>
```

或

```text
implementation group: 'com.gitee.Jmysy', name: 'binlog4j-spring-boot-starter', version: 'latest.version'
```

首先, 在 application.yml / application.properties 中编写 binlog4j 所需配置

```yaml
spring:
  binlog4j:
    redis-config:
      host: 127.0.0.1
      port: 6379
      password: taoren@123
    client-configs:
      master:
        username: root
        password: taoren@123
        host: 127.0.0.1
        port: 3306
        serverId: 1990
      slave:
        username: root
        password: taoren@123
        host: 127.0.0.1
        port: 3307
        serverId: 1991

```



### 配置说明

- timeOffset 时间偏移量, 单位：毫秒
- serverId 编号
- redisConfig Redis 配置信息, 详见 RedisConfig
- inaugural 首次启动, 如果为 true 在启动时不再读取 Redis 记录
- persistence 是否启用持久化, 默认为 false
- strict 严格模式, 默认为 true
- mode 模式, 详见: BinlogClientMode
- username 数据库账户
- password 数据库密码
- host 数据库所在服务器 IP 地址
- port 数据库占用端口, 默认 3306
- hikariConfig 数据库连接池配置
